# PROGRAMMED BY ORCA
# Run using python3 
from asyncio.windows_events import NULL
from dataclasses import replace
import random
import string

ARRAYSIZE = 365 # size of the arrays to generate, for bigger number, it will build a bigger arrays, and more bytes will be generated
KEYSIZE = 32

def get_random_string(length): # generate random strings 
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

def hash_djb2(s): # djb2 hashing function                                                                                                                               
    hash = 103126615
    for x in s:
        hash = (( hash << 19) + hash) + ord(x)
    return hash & 0xFFFFFFFF


# im not good in writing python scripts, but this worked just fine:

#generating the random char array && its hashed bytes :::
print("const char *RandomKeys[ArraySize] = { ", end='')
i = 0
String2 = ""
Array = []
while i < ARRAYSIZE:
    RR = get_random_string(2)
    String1 = "\"" + RR + "\", "
    i = i + 1
    if i < ( ARRAYSIZE - 1 ):
        print(String1, end='')
        String2 = str(String2) + hex(hash_djb2(RR)) +  " ,"
        Array.append(RR)
    if i ==  ( ARRAYSIZE - 1 ):
        print(String1.replace(',' ,''), end='') 
        String2 = str(String2) + hex(hash_djb2(RR))
        Array.append(RR)
print(" }; ")

# printing the hashed bytes function :::
print("DWORD32 KeyHashes[ArraySize] = { ", end='')
print(String2 , end='')
print(" }; ")

# picking up random elements from the array we created to be our 32 byte key 
j = 0
keyChosen = ""
keyhash = "" 
while j < KEYSIZE:
    n = random.randint(1,99)
    keyChosen = keyChosen + Array[n]
    keyhash = str(keyhash) + hex(hash_djb2(Array[n])) + ", "
    j = j + 2

# printing it :
print("//key : " , keyChosen)
keyhash = keyhash[:-2]  
print("DWORD32 keyhash[KeySize] = { ", keyhash ,"}; ")

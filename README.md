# AES Brute Forcer
#### If you think normal ways of decryption of your payload isnt somehow enough, you can now brute force it ;)

# How Does it Work
* first of all i wanna note that this way doesnt work `only` for aes encryption algorithms, but for any key you wanna hide (in case it has even length )
* first, a 365 const char array is generated using the python script, each element is 2 charachters, this array will be the base of the next steps
* we then build a array containing the `djb2` hash of the 1st array we generated, each element is 1 `dword32` 
* to generate the key, we pick (sizeofkey / 2) elements from the 1st array, so for example if the key is 16 bytes long we pick 8 random elements 
* at last we generate the `djb2` hashes of these elements, (8 elements = 8 hashes), these hashes will be stored as a 3rd array, we will reference to it in brute forcing 
* Now in C, we loop through the hashed arrays looking for the same hashes, in case of a match, the same index of the element will be the 2 charachters in the char array, that is a part of our key, we can append based on the index of these 2 bytes in the key generated (long story here)
* but we make it, and now we have the original key, i added a x64 calc.exe shellcode generated by metasploit to test it
* and i used my older repo [AESShellEnc](https://gitlab.com/ORCA666/aesshellenc) to save time and show some results

# USAGE:
* first run the python script using python 3, it will output the three arrays i mentioned, plus a 'c style commented key'
* paste the all the 3 arrays [here](https://gitlab.com/ORCA666/aesbruteforcing/-/blob/main/AesBruteForcing/Source.c#L21)
* then use the generated key as your encryption key, paste it in [here](https://gitlab.com/ORCA666/aesbruteforcing/-/blob/main/ShellcodeEnc/main.c#L83) and ofcourse, your iv and shellcode, refere to my older [repo](https://gitlab.com/ORCA666/aesshellenc) for more detailed steps
* after running you will have an encrypted shellcode, u can paste it, and run, the key should be pasted into screen and the shellcode should be decrypted and executed !

# Demo:
![image](https://gitlab.com/ORCA666/aesbruteforcing/-/raw/main/images/demo.png)

# THANKS FOR:
* [tiny-AES-c](https://github.com/kokke/tiny-AES-c) 
* [@CaptMeelo](https://twitter.com/CaptMeelo?s=20&t=566_vB8-aOnbTgNzOgDNOw) for this nice [blog](https://captmeelo.com/redteam/maldev/2022/02/16/libraries-for-maldev.html) that discussed more things to mess with, including the implementation of tiny-AES-c as a functionality to do shellcode encryption.



<h6 align="center"> <i>#                                   STAY TUNED FOR MORE</i>  </h6> 
![120064592-a5c83480-c075-11eb-89c1-78732ecaf8d3](https://gitlab.com/ORCA666/kcthijack/-/raw/main/images/PP.png)


